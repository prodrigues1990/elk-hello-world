import logging
import os
import requests
from celery import Celery
from elasticsearch import Elasticsearch
from time import sleep

VATSIM_STATUS_URL = r'http://cluster.data.vatsim.net/vatsim-data.json'
logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
celery = Celery('pipeline', broker='redis://redis')

def pool():
    data = requests.get(VATSIM_STATUS_URL).json()
    pilots = list(filter(lambda c: c['clienttype'] == 'PILOT', data['clients']))
    controllers = list(filter(lambda c: c['clienttype'] == 'ATC', data['clients']))
    logging.info(f'pooled {len(pilots)} pilots, {len(controllers)} controllers')
    index.apply_async([pilots, controllers])

@celery.task
def index(pilots, controllers):
    es = Elasticsearch('http://elasticsearch:9200')

    for pilot in pilots:
        es.index(index='pilots', body=pilot)

    for controller in controllers:
        es.index(index='controllers', body=controller)

    logging.info(f'indexed {len(pilots)} pilots, {len(controllers)} controllers')

if __name__ == '__main__':
    while True:
        pool()
        sleep(30)
